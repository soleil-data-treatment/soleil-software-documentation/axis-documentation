# Aide et ressources de AXIS pour Synchrotron SOLEIL

[<img src="http://unicorn.mcmaster.ca/img/top_right.gif" width="150"/>](http://unicorn.mcmaster.ca/axis/aXis2000-download.html)

## Résumé

- Imagerie hyperspectral
- Modules d'analyse de stat, correction
ROI, extraction de spectres
- Logiciel très connu et très bien supporté  
- Privé

## Sources

- Code source:  Il faut s'enregistrer pour avoir le run time
- Documentation officielle: http://unicorn.mcmaster.ca/axis/aXis2000-IDLVM.html sur un pdf dans le download

## Navigation rapide

| Tutoriaux |
| - |
| [Tutoriel d'installation officiel](http://unicorn.mcmaster.ca/axis/aXis2000-download.html) |
| [Tutoriaux officiels](http://unicorn.mcmaster.ca/axis/axis2000-tutorial.zip) |

## Installation

- Systèmes d'exploitation supportés: Windows
- Installation: Difficile (très technique),  nécessite une machine virtuelle windows

## Format de données

- en entrée: hdf5,  fichiers images: jpeg,  tif
fichiers textes
- en sortie: données volumiques hyperspectrales
ncb (format binaire)
- sur la Ruche,  sur la Ruche locale
